# -*- coding: utf-8 -*-
# :Project:   WP -- Wordpress quick&dirty setup
# :Created:   gio 01 mar 2018 11:33:44 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 eTour s.r.l.
#

export TOPDIR := $(CURDIR)
export VENVDIR := $(TOPDIR)/env
export PYTHON := $(VENVDIR)/bin/python
export SHELL := /bin/bash
export SYS_PYTHON := $(shell which python3.6 || which python3)

ACTIVATE_SCRIPT := $(VENVDIR)/bin/activate
PIP := $(VENVDIR)/bin/pip
REQUIREMENTS ?= requirements.txt
REQUIREMENTS_TIMESTAMP := $(VENVDIR)/$(REQUIREMENTS).timestamp


all: virtualenv help


help::
	@printf "\nVirtualenv\n==========\n"

help::
	@printf "\nvirtualenv\n\tsetup the Python virtualenv and install required packages\n"

.PHONY: virtualenv
virtualenv: $(VENVDIR) requirements

$(VENVDIR):
	@printf "Bootstrapping Python 3 virtualenv...\n"
	@$(SYS_PYTHON) -m venv --prompt $(notdir $(TOPDIR)) $@
	@$(MAKE) -s upgrade-pip

help::
	@printf "\nupgrade-pip\n\tupgrade pip\n"

.PHONY: upgrade-pip
upgrade-pip:
	@printf "Upgrading pip...\n"
	@$(PIP) install --no-cache-dir --upgrade pip

help::
	@printf "\nrequirements\n\tinstall/update required Python packages\n"

.PHONY: requirements
requirements: $(REQUIREMENTS_TIMESTAMP)

$(REQUIREMENTS_TIMESTAMP): $(REQUIREMENTS)
	@printf "Installing pre-requirements...\n"
	@PATH=$(TOPDIR)/bin:$(PATH) $(PIP) install --no-cache-dir -r $(REQUIREMENTS)
	@touch $@

distclean::
	rm -rf $(VENVDIR)


DC := $(VENVDIR)/bin/docker-compose
DCRUN := $(DC) run --rm

help::
	@printf "\nCompose\n=======\n"

help::
	@printf "\nstart\n\tstart the services\n"

.PHONY: start
start:
	@printf "A bit of patience, then visit http://localhost:8088/\n"
	@$(DC) up

help::
	@printf "\nclean\n\tremove all the containers\n"

.PHONY: clean
clean:
	@$(DC) down
