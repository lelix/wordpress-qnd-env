# -*- coding: utf-8 -*-
# :Project:   WP -- Sugarless virtualenv starter
# :Created:   gio 01 mar 2018 11:46:56 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 eTour s.r.l.
#

# Better safe than sorry
if [ ! -f env/bin/activate ]
then
    make virtualenv
fi

source env/bin/activate
