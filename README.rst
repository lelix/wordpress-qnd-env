.. -*- coding: utf-8 -*-
.. :Project:   WP -- Short notes
.. :Created:   gio 01 mar 2018 12:02:17 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 eTour s.r.l.
..

=========================================
 Wordpress quick&dirty environment setup
=========================================

Questo repository contiene le istruzioni di base per installare una istanza di Wordpress
agganciato ad un database MySQL, ciascuno in un suo container Docker.

Una volta fatto il clone del repository, con

::

  $ git clone git@gitlab.com:etour/wordpress-qnd-env.git wordpress

bisogna creare il *virtualenv* Python 3::

  $ cd wordpress
  $ make virtualenv

A questo punto, ogni volta che si desidera operare in questo ambiente bisogna attivarlo::

  $ source env.sh

Per far partire l'ambaradan basta un

::

  $ make start

.. warning:: La prima esecuzione impiega qualche minuto perché deve creare il database, e
             con ogni probabilità il servizio *Wordpress* andrà in timeout... in tal caso,
             **dopo aver pazientato un pochino per essere sicuri che sia quello il
             problema**, bisogna stoppare la fazenda con un Ctrl-C e rifare il ``make
             start``.

Una volta che i servizi sono attivati correttamente, visitare l'indirizzo
http://localhost:8088/ e procedere...

Per ripartire da zero::

  $ make clean

rimuove i container **e i volumi associati**, quindi il successivo ``start`` ricreerà il
tutto...
